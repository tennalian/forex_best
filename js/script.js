$(function() {
	$('#dg-container').gallery();
});

$(function(){
	var partner = getParameterByName('p');
	console.log(getParameterByName('p'));
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	if (partner) {
		$('a').each(function(){
			var href = $(this).attr('href');
			href = href.replace('\/p4\/','\/p'+partner+'\/');
			$(this).attr('href',href);
		})
	}
});